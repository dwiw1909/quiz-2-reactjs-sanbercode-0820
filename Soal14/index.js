const volumeBalok = (...arg) => {
  let res = arg.reduce((acc, value) => acc * value)
  return `Volume baloknya adalah ${res}`
}

const volumeKubus = (...arg) => {
  let res = arg.reduce((acc, value) => acc * value)
  return `Volume kubusnya adalah ${res}`
}

console.log(volumeBalok(1, 2, 3));
console.log(volumeKubus(3, 3, 3));
