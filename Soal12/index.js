class BangunDatar {
  constructor(){
    this._nama = null
  }
  get nama() {
    return this._nama
  }
  set nama(nama) {
    this._nama = nama
  }
  luas() {
    return ''
  }
  keliling() {
    return ''
  }
}

class Lingkaran extends BangunDatar {
  constructor() {
    super()
  }
  luas(jari) {
    return (22/7) * (jari*jari)
  }
  keliling(jari) {
    return 2 * (22/7) * jari
  }
}

class Persegi extends BangunDatar {
  constructor(){
    super()
  }
  luas(sisi){
    return sisi * sisi
  }
  keliling(sisi){
    return 4 * sisi
  }
}

const lingkaran = new Lingkaran()
console.log(lingkaran.luas(6));
console.log(lingkaran.keliling(7));

const persegi = new Persegi()
console.log(persegi.luas(4));
console.log(persegi.keliling(5));
