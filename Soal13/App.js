import React from 'react';
import './App.css'
import Item from './Item'

const App = () => {
  const data = [
    {name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"},
    {name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"},
    {name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"},
    {name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }
  ]

  return (
    <div className='container'>
      {data.map(data => <Item data={data} />)}
    </div>
  );
}

export default App;

// =====kode css===== atau di file App.css
// .container {
//   width: 600px;
//   margin: 50px auto;
//   display: flex;
//   justify-content: center;
//   flex-wrap: wrap;
// }

// .item {
//   width: 250px;
//   height: 280px;
//   border: 1px solid black;
//   border-radius: 10px;
//   margin: 10px;
// }
