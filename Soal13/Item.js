import React from 'react'

const Item = ({data}) => {
  const panggilan = () => {
    if(data.gender === 'Male'){
      return 'Mr'
    } else if (data.gender === 'Female'){
      return 'Mrs'
    }
  }
  return (
    <div className='item'>
      <img src={data.photo} style={{height: '50%', width:' 100%', borderRadius: '10px 10px 0px 0px'}} />
      <div style={{margin: '5px'}}>
        <p><b>{`${panggilan()} ${data.name}`}</b></p>
        <p>{data.profession}</p>
        <p>{`${data.age} years old`}</p>
      </div>
    </div>
  )
}

export default Item
